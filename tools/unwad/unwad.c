// unwad: unpacks doom2d wads, exports images, sucks your cock
// also transforms cyrillic names

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <unistd.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../common/stb_image_write.h"

#ifdef _WIN32
# define fmkdir(x, p) mkdir(x)
#else
# define fmkdir(x, p) mkdir(x, p)
#endif

static uint32_t playpal[256] = { 0 };

char tr_upper[] = "ABVGDEJZIYKLMNOPRSTUFHCCSSXIQEUA";
char tr_lower[] = "abvgdejziyklmnoprstufhccssxiqeua";

// 866 -> translit and weird char replacement
static void lmp_fixname(char *s, int n) {
  int i = 0;
  for (unsigned char *c = (unsigned char *)s; i < n && *c; ++c, ++i) {
    if (*c >= 0x80 && *c < 0xB0)
      *c = tr_upper[*c - 0x80];
    else if (*c >= 0xE0 && *c < 0xF0)
      *c = tr_lower[*c - 0xE0];
    else if (*c == 0xF0)
      *c = 'E';
    else if (*c == 0xF1)
      *c = 'e';
    else if (*c == '\\' || *c == '*')
      *c = '_';
  }
}

static char *lmp_read(FILE *fin, wad_entry_t *lmp) {
  // who cares
  char *buf = malloc(lmp->len);
  if (!buf) {
    perror("lmp_read(): malloc()");
    return NULL;
  }
  fseek(fin, lmp->ofs, SEEK_SET);
  size_t rx = fread(buf, lmp->len, 1, fin);
  if (!rx) {
    perror("lmp_read(): fread()");
    free(buf);
    return NULL;
  }
  return buf;
}

static int lmp_extract(FILE *fin, wad_entry_t *lmp, const char *path) {
  char fname[2048];
  snprintf(fname, sizeof(fname), "%s/%.8s.lmp", path, lmp->name);

  char *buf = lmp_read(fin, lmp);
  if (!buf) return -1;

  FILE *fout = fopen(fname, "wb");
  if (!fout) {
    perror(fname);
    free(buf);
    return -2;
  }

  fwrite(buf, lmp->len, 1, fout);

  fclose(fout);
  free(buf);
  return 0;
}

static int lmp_export_image(FILE *fin, wad_entry_t *lmp, const char *path) {
  char fname[2048];
  snprintf(fname, sizeof(fname), "%s/%.8s.png", path, lmp->name);

  d2d_image_t *img = (d2d_image_t *)lmp_read(fin, lmp);
  if (!img) return -1;

  if (!img->w || !img->h) {
    fprintf(stderr, "invalid image in `%.8s`?\n", lmp->name);
    free(img);
    return -2;
  }

  uint32_t *out = calloc(1, sizeof(uint32_t) * img->w * img->h);
  if (!out) {
    perror("lmp_export_image(): calloc()");
    free(img);
    return -3;
  }

  uint8_t *pin = &img->data[0];
  uint32_t *pout = out;
  for (int i = 0; i < img->w*img->h; ++i, ++pin, ++pout)
    *pout = playpal[*pin];

  int ret = !stbi_write_png(fname, img->w, img->h, 4, out, img->w * sizeof(uint32_t));

  free(out);
  free(img);
  return ret;
}

static int lmp_export_sound(FILE *fin, wad_entry_t *lmp, const char *path) {
  char fname[2048];
  snprintf(fname, sizeof(fname), "%s/%.8s.wav", path, lmp->name);

  d2d_sound_t *snd = (d2d_sound_t*)lmp_read(fin, lmp);
  if (!snd) return -1;

  if (!snd->len) {
    fprintf(stderr, "invalid sound in `%.8s?\n", lmp->name);
    free(snd);
    return -2;
  }

  // this happens in some DMIs for some reason
  if (snd->len > lmp->len - sizeof(d2d_sound_t)) {
    printf("     warning: snd->len %u > lmp->len\n", (uint32_t)snd->len);
    snd->len = lmp->len - sizeof(d2d_sound_t);
  }

  // unsigned pcm -> signed pcm
  for (int i = 0; i < snd->len; ++i)
    snd->data[i] = (uint8_t)((int8_t)snd->data[i] - 0x80);

  FILE *fout = fopen(fname, "wb");
  if (!fout) {
    fprintf(stderr, "could not open `%s` for writing\n", fname);
    free(snd);
    return -3;
  }

  uint32_t val;

  // write a WAV

  fwrite("RIFF", 4, 1, fout);
  val = snd->len + 44 - 8;
  fwrite(&val, 4, 1, fout); // bytes remaining
  fwrite("WAVE", 4, 1, fout);
  fwrite("\x66\x6d\x74\x20", 4, 1, fout); // "fmt "
  fwrite("\x10\x00\x00\x00", 4, 1, fout); // 16 = PCM subchunk size
  fwrite("\x01\x00", 2, 1, fout); // 01 = format is PCM
  fwrite("\x01\x00", 2, 1, fout); // 01 = mono
  val = snd->rate;
  fwrite(&val, 4, 1, fout);       // sample rate
  fwrite(&val, 4, 1, fout);       // bytes per sec: sample rate * ch * sample len
  fwrite("\x01\x00", 2, 1, fout); // bytes per sample: sample len * ch
  fwrite("\x08\x00", 2, 1, fout); // bits per sample: 8-bit PCM
  fwrite("data", 4, 1, fout);
  val = snd->len;
  fwrite(&val, 4, 1, fout);
  fwrite(&snd->data[0], snd->len, 1, fout);

  fclose(fout);
  free(snd);
  return 0;
}

static inline int lmp_export(FILE *fin, wad_entry_t *lmp, char type, const char *path) {
  switch (type) {
    case 'S':
    case 'W':
      return lmp_export_image(fin, lmp, path);
    case 'D':
      if (lmp->name[0] == 'D' && lmp->name[1] == 'S')
        return lmp_export_sound(fin, lmp, path); // otherwise it's a different sound (VOC?)
      else
        return lmp_extract(fin, lmp, path);
    case 'M':
      // HACK: export DMI instruments as WAV
      if (!strncmp(lmp->name, "DMI0", 4))
        return lmp_export_sound(fin, lmp, path);
      // fallthrough
    default:
      return lmp_extract(fin, lmp, path);
  }
}

static inline void lmp_loadpal(FILE *fin, wad_entry_t *lmp, uint32_t *pal) {
  uint8_t rgb[256][3];

  fseek(fin, lmp->ofs, SEEK_SET);
  fread(rgb, lmp->len, 1, fin);

  pal[0] = 0; // transparent color
  for (int i = 1; i < 256; ++i) {
    // doom2d colors are 6-bit
    rgb[i][0] <<= 2; rgb[i][1] <<= 2; rgb[i][2] <<= 2;
    pal[i] = (0xFF << 24) | (rgb[i][2] << 16) | (rgb[i][1] << 8) | rgb[i][0];
  }
}

static inline int lmp_isrootimg(wad_entry_t *lmp) {
  static const char *prefixes[] = {
    "TITLEPIC", "INTERPIC", "ENDPIC", "STBF_", "STCFN",
    "WICOLON", "WIMINUS", "WINUM", "WIPCNT", "M_L", "M_T",
  };
  for (int i = 0; i < sizeof(prefixes) / sizeof(prefixes[0]); ++i)
    if (!strncmp(lmp->name, prefixes[i], strlen(prefixes[i])))
      return 1;
  return 0;
}

int main(int argc, char **argv) {
  if (argc < 3 || argc > 4) {
    printf("usage: %s <infile> <outdir> [--convert]\n", argv[0]);
    return 1;
  }

  int convert = (argc == 4) && !strncmp(argv[3], "--convert", 9);

  FILE *f = fopen(argv[1], "rb");
  if (!f) {
    perror("can't open wad");
    return 1;
  }

  wad_header_t hdr;
  fread(&hdr, sizeof(hdr), 1, f);

  if (!strncmp(hdr.magic, "PWAD", 4))
    printf("PWAD detected\n");
  else if (!strncmp(hdr.magic, "IWAD", 4))
    printf("IWAD detected\n");
  else {
    fprintf(stderr, "file is not a valid WAD\n");
    fclose(f);
    return 1;
  }

  printf("table ofs = %d, num lumps = %d\n", hdr.tableofs, hdr.numlumps);
  fseek(f, hdr.tableofs, SEEK_SET);

  wad_entry_t lumps[hdr.numlumps];
  fread(lumps, sizeof(wad_entry_t), hdr.numlumps, f);

  char path[2048];
  strncpy(path, argv[2], sizeof(path));

  char section = '/';

  for (int i = 0; i < hdr.numlumps; ++i) {
    lmp_fixname(lumps[i].name, sizeof(lumps[i].name));
    printf("%04d %8.8s ofs %08d len %05d\n", i, lumps[i].name, lumps[i].ofs, lumps[i].len);

    if (lumps[i].len == 0) {
      if (lumps[i].name[2] == 'E') {
        // end section, return to base folder
        strncpy(path, argv[2], sizeof(path));
      } else if (lumps[i].name[2] == 'S') {
        // start section, make a subfolder
        switch (lumps[i].name[0]) {
          case 'M':
            snprintf(path, sizeof(path), "%s/MUSIC", argv[2]);
            break;
          case 'W':
            snprintf(path, sizeof(path), "%s/WALLS", argv[2]);
            break;
          case 'S':
            snprintf(path, sizeof(path), "%s/SPRITES", argv[2]);
            break;
          case 'D':
            snprintf(path, sizeof(path), "%s/SOUNDS", argv[2]);
            break;
          default:
            continue;
        }
        section = lumps[i].name[0];
        fmkdir(path, 0755);
      }
      continue;
    }

    if (!strncmp(lumps[i].name, "PLAYPAL", 7)) {
      lmp_loadpal(f, lumps + i, playpal);
      lmp_extract(f, lumps + i, path);
    } else if (convert) {
      lmp_export(f, lumps + i, lmp_isrootimg(lumps + i) ? 'W' : section, path);
    } else {
      lmp_extract(f, lumps + i, path);
    }
  }

  fclose(f);
  return 0;
}
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#ifdef _WIN32
# define fmkdir(x, p) mkdir(x)
#else
# define fmkdir(x, p) mkdir(x, p)
#endif

#define STB_IMAGE_IMPLEMENTATION
#include "../common/stb_image.h"

/****************************************************/
/* DOOM 2D MD MAP FORMAT V2                         */
/* shit like numth/numsw defined in code            */
/****************************************************/
/* u8 walls[100*100];      // tile type array, RAM  */
/* u8 switchmap[100*100];  // tile->switch map, ROM */
/* u16 tiles[100*100];     // tilemap, ROM          */
/* switch_t sw[numsw];     // trigger array, RAM    */
/* mapthing_t th[numth];   // thing array, ROM      */
/****************************************************/

#define ROUND_UP(N, S) ((((N) + (S) - 1) / (S)) * (S))

#define TILE_USERINDEX         16
#define TILE_ATTR_PRIORITY_SFT 15
#define TILE_ATTR_PALETTE_SFT  13
#define TILE_ATTR_VFLIP_SFT    12
#define TILE_ATTR_HFLIP_SFT    11

#define TILE_ATTR_FULL(pal, prio, flipV, flipH, index) \
  (((flipH) << TILE_ATTR_HFLIP_SFT) + ((flipV) << TILE_ATTR_VFLIP_SFT) + \
  ((pal) << TILE_ATTR_PALETTE_SFT) + ((prio) << TILE_ATTR_PRIORITY_SFT) + (index))

#define PALETTE_COLOR(r, g, b) (((r) >> 4) & 0x00E | ((g) & 0x0E0) | ((b) << 4) & 0xE00)

#define MAPW 100
#define MAPH 100
#define MAX_SWITCHES 100
#define MAX_THINGS 1024

#define MAPMAGIC "Doom2D\x1A"

#pragma pack(push, 1)
typedef struct {
  char id[8];
  int16_t ver;
} map_header_t;

typedef struct {
  int16_t t;
  int16_t st; 
  int32_t sz;
} map_block_t;

typedef struct {
  char name[8];
  char transparent;
} map_texture_t;

typedef struct {
  uint8_t x, y;
  uint8_t t;
  uint8_t tm;
  uint8_t a, b;
  uint8_t c, d;
  uint8_t f;
} map_switch_t;

typedef struct {
  int16_t x, y;
  int16_t t;
  uint16_t f;
} map_thing_t;
#pragma pack(pop)

enum mapblocktype {
  MB_COMMENT = -1,
  MB_END = 0,
  MB_WALLNAMES,
  MB_BACK,
  MB_WTYPE,
  MB_FRONT,
  MB_THING,
  MB_SWITCH,
  MB_MUSIC,
  MB_SKY,
  MB_SWITCH2,
};

enum switchtype {
  SW_NONE,
  SW_EXIT,
  SW_EXITS,
  SW_OPENDOOR,
  SW_SHUTDOOR,
  SW_SHUTTRAP,
  SW_DOOR,
  SW_DOOR5,
  SW_PRESS,
  SW_TELE,
  SW_SECRET,
  SW_LIFTUP,
  SW_LIFTDOWN,
  SW_TRAP,
  SW_LIFT
};

#define MAX_TILES 2048
#define TILEW 8
#define TILEH 8

typedef struct {
  uint8_t color[TILEH][TILEW]; /* 0..15 */
} smd_tile_t;

#define MAX_TEXW 256
#define MAX_TEXH 256
#define MAX_MEGATILES 2048
#define MTW (MAX_TEXW / 8)
#define MTH (MAX_TEXH / 8)

typedef struct {
  int w, h;
  struct {
    char palette;
    int tile;
  } tiles[MTH][MTW];
} smd_megatile_t;

// palettes 0 and 1 are global, 2 and 3 are per-map
static uint32_t pal[4][16];

static smd_tile_t tiles[MAX_TILES];
static int numtiles = 0;

static smd_megatile_t mtex[256];
static map_texture_t tex[256];
static int numtex = 0;

static map_switch_t sw[MAX_SWITCHES];
static int numsw = 0;

static map_thing_t things[MAX_THINGS];
static int numth = 0;

static uint8_t walls[MAPH][MAPW] = { 0 };
static uint8_t switchmap[MAPH][MAPW] = { 0xFF };
static uint8_t front[MAPH][MAPW] = { 0 };
static uint8_t back[MAPH][MAPW] = { 0 };
static uint8_t tmp[MAPW * MAPH * 2] = { 0 };

static uint16_t tilemap[MAPH][MAPW];

typedef struct {
  int w, h, ch;
  uint32_t *data;
} img_t;

char tr_upper[] = "ABVGDEJZIYKLMNOPRSTUFHCCSSXIQEUA";
char tr_lower[] = "abvgdejziyklmnoprstufhccssxiqeua";

// 866 -> translit (for texture names)
static void fixname(char *s, int n) {
  int i = 0;
  for (unsigned char *c = s; i < n && *c; ++c, ++i) {
    if (*c >= 0x80 && *c < 0xB0)
      *c = tr_upper[*c - 0x80];
    else if (*c >= 0xE0 && *c < 0xF0)
      *c = tr_lower[*c - 0xE0];
    else if (*c == 0xF0)
      *c = 'E';
    else if (*c == 0xF1)
      *c = 'e';
  }
}

static void fatal (const char *fmt, ...) {
  va_list arg;
  va_start(arg, fmt);
  fprintf(stderr, "fatal: ");
  vfprintf(stderr, fmt, arg);
  fprintf(stderr, "\n");
  va_end(arg);
  exit(1);
}

static void img_round_up(img_t *img) {
  int aw = ROUND_UP(img->w, 8);
  int ah = ROUND_UP(img->h, 8);
  printf("  was %dx%d, resizing to %dx%d\n", img->w, img->h, aw, ah);
  uint32_t *ndata = calloc(1, aw*ah*4);
  if (!ndata) fatal("out of memory resizing image");
  uint32_t *pout = ndata + (aw / 2 - img->w / 2);
  for (int y = 0; y < img->h; ++y, pout += aw)
    memcpy(pout, img->data + y*img->w, img->w*4);
  stbi_image_free(img->data);
  img->data = ndata;
  img->w = aw;
  img->h = ah;
}

static img_t *img_load (const char *fname) {
  img_t *img = calloc(1, sizeof(img_t));
  if (!img) fatal("out of memory loading `%s`", fname);
  // load image and convert to ARGB8888
  img->data = (uint32_t *)stbi_load(fname, &img->w, &img->h, &img->ch, 4);
  if (!img->data) { free(img); fatal("stbi_load(%s): %s", fname, stbi_failure_reason()); }
  return img;
}

static void img_free (img_t *img) {
  stbi_image_free(img->data);
  free(img);
}

static uint32_t fix_color (uint32_t color) {
  return color >> 24 != 0 ? color | (0xff << 24) : 0;
}

static void load_palette (const char *file, int ncolors, uint32_t *out) {
  img_t *s = img_load(file);
  if (!s)
    fatal("%s", stbi_failure_reason());
  const int npals = ncolors / 16;
  if (s->w != 16 && s->h != npals)
    fatal("palette size must be 16x%d pixels", npals);
  printf("palette: %i\n", s->w);
  for (int i = 0; i < npals; i++) {
    for (int j = 0; j < 16; j++) {
      out[i*16 + j] = s->data[i*16 + j];
      printf(" 0x%08x", out[i*16 + j]);
    }
    printf("\n");
  }
  printf("fixed:\n");
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 16; j++) {
      out[i*16 + j] = fix_color(out[i*16 + j]);
      printf(" 0x%08x", out[i*16 + j]);
    }
    printf("\n");
  }
  img_free(s);
}

static inline uint32_t get_pixel (const img_t *s, int x, int y) {
  assert(s);
  assert(x >= 0);
  assert(y >= 0);
  assert(x < s->w);
  assert(y < s->h);
  uint32_t *p = s->data + y * s->w;
  uint32_t rgb = *(p + x);
  return fix_color(rgb);
}

static int find_color (uint32_t color, int *p) {
  assert(p);
  if (color == 0)
    return 0;
  for (int i = 0; i < 4; i++) {
    for (int j = 1; j < 16; j++) {
      if (pal[i][j] == color) {
        *p = i;
        return j;
      }
    }
  }
  *p = -1;
  return -1;
}

static void fill_tile (const img_t *s, int x, int y, int *p, smd_tile_t *t) {
  assert(s);
  assert(p);
  assert(t);
  assert(x >= 0);
  assert(y >= 0);
  assert(x * TILEW < s->w - TILEW + 1);
  assert(y * TILEH < s->h - TILEH + 1);
  int maxi = s->w - x * TILEW >= TILEW ? TILEW : s->w % TILEW;
  int maxj = s->h - y * TILEH >= TILEH ? TILEH : s->h % TILEH;
  memset(t, 0, sizeof(smd_tile_t));
  for (int j = 0; j < maxj; j++) {
    for (int i = 0; i < maxi; i++) {
      uint32_t color = get_pixel(s, x * TILEW + i, y * TILEH + j);
      int index = find_color(color, p);
      if (index < 0)
        fatal("no color found at %ix%i (0x%08x)", x * TILEW + i, y * TILEH + j, color);
      t->color[j][i] = index;
    }
  }
}


static int equal_tiles (smd_tile_t *a, smd_tile_t *b) {
  assert(a);
  assert(b);
  for (int j = 0; j < TILEH; j++) {
    for (int i = 0; i < TILEW; i++) {
      if (a->color[j][i] != b->color[j][i]) {
        return 0;
      }
    }
  }
  return 1;
}

static int find_tile (smd_tile_t *t) {
  assert(t);
  for (int i = 0; i < numtiles; i++) {
    if (equal_tiles(&tiles[i], t)) {
      return i;
    }
  }
  return -1;
}

static int add_tile (smd_tile_t *t) {
  assert(t);
  if (numtiles >= MAX_TILES)
    fatal("too many tiles");
  memcpy(&tiles[numtiles], t, sizeof(smd_tile_t));
  numtiles += 1;
  return numtiles - 1;
}

static int dedup_tile (smd_tile_t *t) {
  assert(t);
  int index = find_tile(t);
  if (index < 0)
    index = add_tile(t);
  return index;
}

static void load_texture (int index, const char *path) {
  assert(index >= 0);
  assert(index < numtex);
  char file[2048] = { 0 };
  snprintf(file, sizeof(file), "%s/%.8s.png", path, tex[index].name);
  img_t *s = img_load(file);
  if (!s)
    fatal("%s", stbi_failure_reason());
  // resize image if it's too small or not 8 aligned, placing texture at the top center
  if ((s->w & 7) || (s->h & 7))
    img_round_up(s);
  if (s->w > MAX_TEXW || s->h > MAX_TEXH)
    fatal("texture %s not fit to converter limits", file);
  mtex[index].w = (s->w + 8 - 1) / 8;
  mtex[index].h = (s->h + 8 - 1) / 8;
  for (int j = 0; j < mtex[index].h; j++) {
    for (int i = 0; i < mtex[index].w; i++) {
      int p;
      smd_tile_t t;
      fill_tile(s, i, j, &p, &t);
      mtex[index].tiles[j][i].palette = p;
      mtex[index].tiles[j][i].tile = dedup_tile(&t);
    }
  }
  img_free(s);
}

static void unpack (uint8_t *buf, int len, uint8_t *obuf) {
  uint8_t *p, *o;
  int l, n;
  for (p = buf, o = obuf, l = len; l; ++p, --l) {
    if (*p == 255) {
      n = *((uint16_t *)(++p));
      memset(o, *(p += 2), n);
      o += n;
      l -= 3;
    } else {
      *(o++) = *p;
    }
  }
}

static void read_plane (FILE *f, map_block_t blk, uint8_t *out) {
  if (blk.st == 1) {
    fread(tmp, blk.sz, 1, f);
    unpack(tmp, blk.sz, out);
  } else if (blk.st == 0) {
    fread(out, blk.sz, 1, f);
  }
}

static int read_texnames (FILE *f, map_block_t blk, map_texture_t *out) {
  int num = blk.sz / sizeof(map_texture_t);
  fread(out+1, sizeof(map_texture_t), num, f); // texture 0 is unused
  return num+1;
}

static int read_switches (FILE *f, map_block_t blk, map_switch_t *out) {
  int num = blk.sz / sizeof(map_switch_t);
  fread(out, sizeof(map_switch_t), num, f);
  return num;
}

static int read_things (FILE *f, map_block_t blk, map_thing_t *out) {
  int num = blk.sz / sizeof(map_thing_t);
  fread(out, sizeof(map_thing_t), num, f);
  return num;
}

static inline int min (int a, int b) {
  return a < b ? a : b;
}

static inline void copy_texture_to_map (uint16_t map[][MAPW], int x, int y, int index, unsigned prio) {
  assert(map);
  assert(x >= 0);
  assert(y >= 0);
  assert(x < MAPW);
  assert(y < MAPH);
  for (int j = 0; j < min(mtex[index].h, MAPH - y); j++) {
    for (int i = 0; i < min(mtex[index].w, MAPW - x); i++) {
      int palette = mtex[index].tiles[j][i].palette;
      int tile = mtex[index].tiles[j][i].tile;
      uint16_t fuck = __builtin_bswap16(TILE_ATTR_FULL(palette, prio, 0, 0, tile));
      map[y + j][x + i] = fuck;
    }
  }
}

static inline void fill_door(int sx, int sy) {
  int ssx = sx;
  int ssy = sy;
  int tile = front[ssy][ssx];

  // find x extents of the door
  int ex = sx + 1;
  for (; sx && walls[sy][sx-1] == 3; --sx);
  for (; ex < MAPW && walls[sy][ex] == 3; ++ex);

  // find y extents of the door
  int ey = sy + 1;
  for (; sy && walls[sy-1][ssx] == 3; --sy);
  for (; ey < MAPH && walls[ey][ssx] == 3; ++ey);

  // blast
  for (int y = sy; y < ey; ++y)
  for (int x = sx; x < ex; ++x) {
    if (!front[y][x]) {
      walls[y][x] |= 128;
      front[y][x] = tile;
    }
  }
}

int main(int argc, char **argv) {
  if (argc != 3) {
    printf("usage: %s <infolder> <outfolder>\n", argv[0]);
    return -1;
  }

  numtiles = 1; /* first tile always void */

  char path[2048];
  fmkdir(argv[2], 0755);

  snprintf(path, sizeof(path), "%s/PAL.png", argv[1]);
  load_palette(path, 32, &pal[0][0]);
  load_palette("pal_global.png", 32, &pal[2][0]);

  snprintf(path, sizeof(path), "%s/MAP.lmp", argv[1]);
  FILE *fin = fopen(path, "rb");
  if (!fin) {
    perror("fopen()");
    return -2;
  }

  map_header_t hdr = { 0 };
  fread(&hdr, sizeof(hdr), 1, fin);
  if (strncmp(hdr.id, MAPMAGIC, sizeof(hdr.id))) {
    fprintf(stderr, "invalid map file: %s\n", path);
    fclose(fin);
    return -3;
  }

  // each cell [y][x] in the fg and bg arrays is the number of the texture
  // residing at that position in the map, or 0 for nothing
  // each cell [y][x] in the walltypes array is the tile type
  // (0 - nothing, 1 - wall, 2 - door, 4 - step, etc)

  int load_level = 0;
  map_block_t blk = { 0 };
  while (!feof(fin)) {
    if (fread(&blk, sizeof(blk), 1, fin) <= 0)
      break;
    switch (blk.t) {
      case MB_FRONT:
        load_level++;
        read_plane(fin, blk, &front[0][0]);
        break;
      case MB_BACK:
        load_level++;
        read_plane(fin, blk, &back[0][0]);
        break;
      case MB_WTYPE:
        load_level++;
        read_plane(fin, blk, &walls[0][0]);
        break;
      case MB_WALLNAMES:
        numtex = read_texnames(fin, blk, tex);
        printf("read %d wallnames:\n", numtex);
        for (int i = 0; i < numtex; ++i) {
          fixname(tex[i].name, sizeof(tex[i].name));
          printf("  %.8s\n", tex[i].name);
          if (tex[i].name[0])
            load_texture(i, argv[1]);
        }
        break;
      case MB_SWITCH2:
        numsw = read_switches(fin, blk, sw);
        printf("read %d switches\n", numsw);
        break;
      case MB_THING:
        numth = read_things(fin, blk, things);
        printf("read %d things\n", numth);
        break;
      default:
        fseek(fin, blk.sz, SEEK_CUR);
        break;
    }
  }

  fclose(fin);

  if (load_level != 3) {
    fprintf(stderr, "map file must contain MB_FRONT, MB_BACK and MB_WTYPE\n");
    return -4;
  }

  // set the CLEAR flag on open door tiles and fill the tilemap with the door texture
  for (int i = 0; i < numsw; ++i)
    if (sw[i].t == SW_DOOR || sw[i].t == SW_DOOR5 || sw[i].t == SW_SHUTDOOR ||
        sw[i].t == SW_SHUTTRAP || sw[i].t == SW_TRAP)
      if (walls[sw[i].b][sw[i].a] == 3 && front[sw[i].b][sw[i].a])
        fill_door(sw[i].a, sw[i].b);

  // produce a merged tilemap from the bg and fg arrays

  for (int y = 0; y < MAPH; ++y) {
    for (int x = 0; x < MAPW; ++x) {
      // override bg tiles with fg tiles
      copy_texture_to_map(tilemap, x, y, back[y][x], 0);
      copy_texture_to_map(tilemap, x, y, front[y][x], 1);
    }
  }

  // fill switchmap: 0xFF is empty, otherwise switch number in switches array

  memset(switchmap, 0xFF, sizeof(switchmap));
  for (int i = 0; i < numsw; ++i)
    if (sw[i].t)
      switchmap[sw[i].y][sw[i].x] = i;

  snprintf(path, sizeof(path), "%s/MAP01.bin", argv[2]);
  FILE *fout = fopen(path, "wb");
  if (!fout) {
    perror("fopen()");
    return -5;
  }

  fwrite(walls, sizeof(walls), 1, fout);
  fwrite(switchmap, sizeof(switchmap), 1, fout);
  fwrite(tilemap, sizeof(tilemap), 1, fout);
  fwrite(sw, sizeof(map_switch_t), numsw, fout);
  fwrite(things, sizeof(map_thing_t), numth, fout);

  fclose(fout);

  snprintf(path, sizeof(path), "%s/TEX01.bin", argv[2]);
  fout = fopen(path, "wb");
  if (!fout) {
    perror("fopen()");
    return -6;
  }
  for (int k = 0; k < numtiles; k++) {
    for (int j = 0; j < TILEW; j++) {
      for (int i = 0; i < TILEW; i += 2) {
        fputc(tiles[k].color[j][i] << 4 | tiles[k].color[j][i + 1], fout);
      }
    }
  }
  fclose(fout);

  snprintf(path, sizeof(path), "%s/PAL01.bin", argv[2]);
  fout = fopen(path, "wb");
  if (!fout) {
    perror("fopen()");
    return -7;
  }
  for (int i = 0; i < 2; i++) {
    for (int j = 0; j < 16; j++) {
      uint8_t b = pal[i][j] >> 16;
      uint8_t g = pal[i][j] >> 8;
      uint8_t r = pal[i][j];
      uint16_t color = PALETTE_COLOR(r, g, b);
      fputc(color >> 8, fout);
      fputc(color, fout);
    }
  }
  fclose(fout);

  return 0;
}

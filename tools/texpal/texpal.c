// texpal: take textures from a folder, make a 32-color palette and transform them to that

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <dirent.h>
#include <unistd.h>

#ifdef _WIN32
# define fmkdir(x, p) mkdir(x)
#else
# define fmkdir(x, p) mkdir(x, p)
#endif

#define STB_IMAGE_IMPLEMENTATION
#include "../common/stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../common/stb_image_write.h"

#define PALETTE_COLOR(r, g, b) (((r) >> 4) & 0x00E | ((g) & 0x0E0) | ((b) << 4) & 0xE00)

#define B(c) ((c >> 16) & 0xFF)
#define G(c) ((c >> 8) & 0xFF)
#define R(c) (c & 0xFF)
#define A(c) ((c >> 24) & 0xFF)

#define CLAMP(x, a, b) (((x) < (a)) ? (a) : ((x) > (b)) ? (b) : (x)) 

typedef struct {
  char name[256];
  int w, h, ch;
  uint32_t *data;
} img32_t;

typedef struct {
  int num;
  uint32_t rgb[256];
} pal_t;

typedef struct {
  int w, h, ch;
  uint8_t *data;
  pal_t pal;
} img8_t;

static void fatal (const char *fmt, ...) {
  va_list arg;
  va_start(arg, fmt);
  fprintf(stderr, "fatal: ");
  vfprintf(stderr, fmt, arg);
  fprintf(stderr, "\n");
  va_end(arg);
  exit(1);
}

// pal

static inline void pal_append (pal_t *pal, const uint32_t c) {
  int i;
  for (i = 0; i < pal->num; ++i)
    if (pal->rgb[i] == c)
      break; // color is already in palette
  // color not in palette, append
  if (i == pal->num && pal->num < 256)
    pal->rgb[pal->num++] = c;
}

static pal_t pal_from_img32 (const img32_t *img) {
  // assume all images are 256-color at most
  pal_t pal = { 1, { 0 } }; // first color is transparent
  const uint32_t *p = img->data;
  for (int i = 0; i < img->h*img->w; ++i, ++p) {
    uint32_t c = *p;
    if (A(c) == 0) continue; // transparent pixel, skip
    if (A(c) < 0xFF) c |= 0xFF << 24; // set alpha to 255
    pal_append(&pal, c);
  }
  return pal;
}

static inline pal_t pal_merge (const pal_t *pa, const pal_t *pb) {
  pal_t pm = *pa;
  for (int i = 1; i < pb->num && pm.num < 256; ++i)
    pal_append(&pm, pb->rgb[i]);
  return pm;
}

typedef struct { double rgb[3]; int label; } rgbpt_t;

static inline double rgb_dist (rgbpt_t *a, rgbpt_t *b) {
  double x = a->rgb[0] - b->rgb[0];
  double y = a->rgb[1] - b->rgb[1];
  double z = a->rgb[2] - b->rgb[2];
  return sqrt(x*x + y*y + z*z);
}

static inline int rgb_nearest (rgbpt_t *pt, rgbpt_t *cent, int ncent, double *outd) {
  int mini = 0;
  double mind = rgb_dist(pt, cent + 0);
  for (int i = 1; i < ncent; ++i) {
    double d = rgb_dist(pt, cent + i);
    if (d < mind) {
      mini = i;
      mind = d;
    }
  }
  *outd = mind;
  return mini;
}

static pal_t pal_crunch (const pal_t *pal, int ncols) {
  pal_t out = { 1, { 0 } };

  // decrement because we will add the transparent colors later
  int numtrans = ncols / 16;
  ncols -= numtrans;

  // produce a set of RGB points from palette
  const int npts = pal->num-1;
  rgbpt_t pts[npts];
  for (int i = 0; i < npts; ++i) {
    const uint32_t c = pal->rgb[i+1];
    pts[i].rgb[0] = R(c);
    pts[i].rgb[1] = G(c);
    pts[i].rgb[2] = B(c);
    pts[i].label = 0;
  }

  // compute initial centroids using k++
  rgbpt_t cent[ncols];
  double sqrd[npts];
  cent[0] = pts[rand() % npts];
  cent[0].label = 0;
  for (int n = 1; n < ncols; ++n) {
    double sum = 0.0;
    for (int i = 0; i < npts; ++i) {
      rgb_nearest(pts + i, cent, n, sqrd + i);
      sqrd[i] *= sqrd[i];
      sum += sqrd[i];
    }
    double target = sum * rand() / (RAND_MAX - 1.0);
    int i;
    for (i = 0, sum = sqrd[0]; sum < target; sum += sqrd[++i]);
    cent[n] = pts[i];
    cent[n].label = n;
  }

  // apply lloyd's k means to that shit
  // assign initial labels
  double d = 0.0;
  for (int i = 0; i < npts; ++i)
    pts[i].label = rgb_nearest(pts + i, cent, ncols, &d);

  int mlen[ncols];
  int alive = 1;
  while (alive) {
    // update centroids
    for (int i = 0; i < ncols; ++i) {
      cent[i].rgb[0] = cent[i].rgb[1] = cent[i].rgb[2] = 0.0;
      mlen[i] = 0;
    }
    for (int i = 0; i < npts; ++i) {
      int n = pts[i].label;
      cent[n].rgb[0] += pts[i].rgb[0];
      cent[n].rgb[1] += pts[i].rgb[1];
      cent[n].rgb[2] += pts[i].rgb[2];
      mlen[n]++;
    }
    for (int i = 0; i < ncols; ++i) {
      double inv = 1.0 / (double)mlen[i];
      cent[i].rgb[0] *= inv;
      cent[i].rgb[1] *= inv;
      cent[i].rgb[2] *= inv;
    }
    // reassign labels
    alive = 0;
    for (int i = 0; i < npts; ++i) {
      int imin = rgb_nearest(pts + i, cent, ncols, &d);
      if (imin != pts[i].label) {
        pts[i].label = imin;
        alive = 1; // changes are still happening
      }
    }
  }

  out.num = ncols+numtrans;
  for (int i = 0, k = 0; i < out.num; ++i) {
    // every 16th color is transparent
    if (i & 15) {
      // colors need to be crunched for genesis
      uint8_t r = (uint8_t)CLAMP(cent[k].rgb[0], 0, 255) & 0xE0;
      uint8_t g = (uint8_t)CLAMP(cent[k].rgb[1], 0, 255) & 0xE0;
      uint8_t b = (uint8_t)CLAMP(cent[k].rgb[2], 0, 255) & 0xE0;
      out.rgb[i] = (0xFF << 24) | (b << 16) | (g << 8) | r;
      ++k;
    } else {
      out.rgb[i] = 0;
    }
  }

  return out;
}

static uint8_t pal_find_nearest (const pal_t *pal, const uint32_t c) {
  const int cr = R(c), cg = G(c), cb = B(c);
  uint8_t res = 1;
  int mindist = INT_MAX;
  for (int i = 1; i < pal->num; ++i) {
    const uint32_t pc = pal->rgb[i];
    const int dr = R(pc) - cr, dg = G(pc) - cg, db = B(pc) - cb;
    const int dist = dr*dr + dg*dg + db*db;
    if (dist < mindist) {
      res = i;
      mindist = dist;
    }
  }
  return res;
}

// img32

static img32_t *img32_load (const char *fname, const char *texname) {
  img32_t *img = calloc(1, sizeof(img32_t));
  if (!img) fatal("out of memory loading `%s`", fname);
  // load image and convert to ARGB8888
  img->data = (uint32_t *)stbi_load(fname, &img->w, &img->h, &img->ch, 4);
  if (!img->data) { free(img); fatal("stbi_load(): %s", stbi_failure_reason()); }
  strncpy(img->name, texname, sizeof(img->name));
  return img;
}

static void img32_crunch(img32_t *img, const pal_t *pal) {
  uint32_t *ptr = img->data;
  for (int i = 0; i < img->h*img->w; ++i, ++ptr) {
    const uint32_t c = *ptr;
    if (A(c) == 0) *ptr = pal->rgb[0];
    else *ptr = pal->rgb[pal_find_nearest(pal, c)];
  }
}

static inline void img32_free (img32_t *img) {
  stbi_image_free(img->data);
  free(img);
}

static inline int img_valid_ext (const char *fname) {
  char *e = strrchr(fname, '.');
  if (e)
    return !strcmp(e, ".png") || !strcmp(e, ".bmp") || !strcmp(e, ".tga");
  return 0;
}


int main(int argc, char **argv) {
  if (argc != 3) {
    printf("usage: %s <indir> <outdir>\n", argv[0]);
    return 1;
  }

  fmkdir(argv[2], 0755);

  pal_t base = { 1, { 0 } };
  pal_t imgpal = { 0 };

  int numtex = 0;
  img32_t **intex = NULL;

  char path[2048] = { 0 };
  DIR *d = opendir(argv[1]);
  if (!d) { fatal("could not open `%s`", argv[1]); }
  for (struct dirent *e = readdir(d); e; e = readdir(d)) {
    if (!img_valid_ext(e->d_name)) continue;

    intex = realloc(intex, sizeof(img32_t*)*(numtex+1));
    if (!intex) { closedir(d); fatal("out of memory reading directory"); }

    snprintf(path, sizeof(path), "%s/%s", argv[1], e->d_name);
    img32_t *img = img32_load(path, e->d_name);
    if (!img) { closedir(d); fatal("out of memory reading `%s`", path); }

    imgpal = pal_from_img32(img);
    printf("`%16s`: %3d colors\n", img->name, imgpal.num);
    base = pal_merge(&base, &imgpal);

    intex[numtex++] = img;
  }
  closedir(d);

  printf("read %d textures\n", numtex);
  printf("generated palette has %d colors\n", base.num);

  printf("crunching down to 32 colors...\n");
  base = pal_crunch(&base, 32);

  printf("converting graphics to %d colors...\n", base.num);
  for (int i = 0; i < numtex; ++i) {
    img32_t *isrc = intex[i];
    img32_crunch(isrc, &base);
    snprintf(path, sizeof(path), "%s/%s", argv[2], isrc->name);
    stbi_write_png(path, isrc->w, isrc->h, 4, isrc->data, isrc->w*sizeof(uint32_t));
    img32_free(isrc);
  }

  free(intex);

  printf("writing palette...\n");
  snprintf(path, sizeof(path), "%s/PAL.png", argv[2]);
  stbi_write_png(path, 16, 2, 4, base.rgb, 16*sizeof(uint32_t));

  return 0;
}

#pragma once

#include <genesis.h>

enum tilemapid {
  TILES_MAP01,
};

void tiles_startup(void);
void tiles_load(u16 idx);

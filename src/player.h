#pragma once

#include <genesis.h>
#include "thing.h"

#define MAX_PLAYERS 2

extern u16 num_players;
extern struct thing *th_player[MAX_PLAYERS];

void plr_controls(struct thing *p, u16 btns);

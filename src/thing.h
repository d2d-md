#pragma once

#include <genesis.h>
#include "player.h"

#define MAX_THINGS 512 // MAXMN=200 + MAXITEM=300 + players/projectiles

enum thingclass {
  TH_WORLD,
  TH_PLAYER,
  TH_TEST,
  TH_MAX,
};

enum thingflags {
  THF_MOVING     = 1,
  THF_MONSTER    = 2,
  THF_ITEM       = 4,
  THF_TAKEDAMAGE = 8,
  THF_ONGROUND   = 16,
  THF_GRAVITY    = 32,
  THF_INWATER    = 64,
  THF_REMOVED    = 128, // will get removed on next frame
};

enum moveflags {
  MOVE_HITWALL  = 1,
  MOVE_HITFLOOR = 2,
  MOVE_HITCEIL  = 4,
  MOVE_HITWATER = 8,
  MOVE_HITAIR   = 16,
  MOVE_FALLOUT  = 32,

  MOVE_HIT      = MOVE_HITWALL | MOVE_HITFLOOR | MOVE_HITCEIL,
};

typedef struct {
  u8 class;
  u8 spr;
  s8 r, h;
  s8 offx, offy;
  u16 flags;
} thingclass_t;

typedef struct thing {
  const thingclass_t *def;
  Sprite *spr[MAX_PLAYERS]; // sprites to display on each camera
  u16 flags;
  s8 dir;
  u8 anim, frame;
  s16 ox, oy;     // old coordinates
  s16 x, y;       // coordinates of the bottom center of the hitbox
  s8 vx, vy;
  s8 ax, ay;
  struct thing *prev, *next;
} thing_t;

typedef struct {
  s16 x, y;
  s16 type;
  u16 flags;
}  __attribute__((packed)) mapthing_t;

extern thing_t *g_things;

thing_t *thing_spawn(u16 class, s16 x, s16 y, s16 dir, thing_t *chain);
void thing_free(thing_t *th);

void thing_draw(thing_t *th);
void thing_think(thing_t *th);

void things_load(mapthing_t *th, u16 num);

static inline void things_think(void) {
  thing_t *th = g_things->next;
  do {
    thing_think(th);
    th = th->next;
  } while (th);
}

static inline void things_draw(void) {
  thing_t *th = g_things->next;
  do {
    thing_draw(th);
    th = th->next;
  } while (th);
}

static inline void things_remove(void) {
  thing_t *th = g_things->next;
  thing_t *next;
  do {
    next = th->next;
    if (th->flags & THF_REMOVED)
      thing_free(th);
    th = next;
  } while (th);
}

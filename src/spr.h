#pragma once

#include <genesis.h>

enum spriteid {
  SPR_PLAYER,
};

void spr_startup(void);
Sprite *spr_get(u16 idx, s16 x, s16 y, bool flip);

#pragma once

#include <genesis.h>
#include "thing.h"
#include "switch.h"
#include "player.h"

#define MAPW 100
#define MAPH 100
#define MAPPIXW 800
#define MAPPIXH 800

#define SCREENW 40
#define SCREENH 28
#define SCREENPIXW 320
#define SCREENPIXH 224

#define VIEWW SCREENW
#define VIEWPIXW SCREENPIXW
#define VIEWH ((SCREENH >> 1) - 2)
#define VIEWPIXH ((SCREENPIXH >> 1) - 16)

#define VDPW 64
#define VDPH 32
#define VDPHH (VDPH >> 1)

enum tiletype {
  MAP_NONE = 0,
  MAP_WALL = 1,
  MAP_DOOR = 2,
  MAP_OPENDOOR = 3,
  MAP_STEP = 4,
  MAP_WATER = 5,
  MAP_ACID1 = 6,
  MAP_ACID2 = 7,
  MAP_FORCECLEAR = 128,
};

typedef struct {
  u8 tilemap;
  u16 numsw;
  u16 numth;
  u8 *map;
  u8 *swmap;
  u16 *tiles;
  switch_t *sw;
  mapthing_t *th;
} map_t;

typedef struct {
  u8  planeidx; // BG_A or BG_B
  u16 plane;    // VDP_BG_A or VDP_BG_B
  u16 sy, sh;   // screen offset and height in pixels
  u16 sty, sth; // screen offset and height in tiles
  s16 x, y;     // position in the world in pixels
  u16 tx, ty;   // position in the world in tiles
  u16 flags;    // set to 1 to reupload the entire screen, eg after teleports
} cam_t;

extern u8 g_cells[MAPH*MAPW];
extern map_t *g_map;
extern cam_t g_cam[MAX_PLAYERS];

extern const u16 tile_rowoff[MAPW];

void map_startup(void);
void map_load(map_t *map);
void map_update(void);

void map_clear_tile(u16 tx, u16 ty);
void map_set_tile(u16 tx, u16 ty, u16 tile);
void map_clear_tile_rect(s16 tx, s16 ty, s16 w, s16 h);
void map_set_tile_rect(s16 tx, s16 ty, s16 w, s16 h, u16 tile);

#define MAP_WALLPTR(m) (u8*)(m)
#define MAP_SWMAPPTR(m) (u8*)(m + MAPH*MAPW)
#define MAP_TILEPTR(m) (u16*)(m + 2*MAPH*MAPW)
#define MAP_SWITCHPTR(m) (switch_t*)(m + 4*MAPH*MAPW)
#define MAP_THINGPTR(m, n) (mapthing_t*)(m + 4*MAPH*MAPW + sizeof(switch_t)*n)

#define DEFINE_MAP(tset, nsw, nth, ptr) \
  { tset, nsw, nth, MAP_WALLPTR(ptr), MAP_SWMAPPTR(ptr), MAP_TILEPTR(ptr), MAP_SWITCHPTR(ptr), MAP_THINGPTR(ptr, nsw) }

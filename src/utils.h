#pragma once

#include <genesis.h>

extern u16 g_ticker;

static inline int Z_sign(int a) {
  if (a > 0)
    return 1;
  if (a < 0)
    return -1;
  return 0;
}

static inline int Z_dec(int a, int b) {
  if (a > b)
    return a - b;
  if (a < -b)
    return a + b;
  return 0;
}

static inline s16 Z_lerp(s16 a, s16 b, s16 t) {
  return (a * (0xFF - t) + b * t) >> 8;
}

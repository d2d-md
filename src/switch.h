#pragma once

#include <genesis.h>

#define MAX_SWITCHES 100

#define SWF_PL_PRESS 1
#define SWF_MN_PRESS 2
#define SWF_PL_NEAR 4
#define SWF_MN_NEAR 8
#define SWF_KEY_R 16
#define SWF_KEY_G 32
#define SWF_KEY_B 64

enum switchtype {
  SW_NONE,
  SW_EXIT,
  SW_EXITS,
  SW_OPENDOOR,
  SW_SHUTDOOR,
  SW_SHUTTRAP,
  SW_DOOR,
  SW_DOOR5,
  SW_PRESS,
  SW_TELE,
  SW_SECRET,
  SW_LIFTUP,
  SW_LIFTDOWN,
  SW_TRAP,
  SW_LIFT
};

typedef struct {
  u8 x, y;
  u8 type;
  u8 tm;
  u8 a, b, c, d;
  u8 flags;
} __attribute__((packed)) switch_t;

void sw_load(switch_t *sw, u16 num);
void sw_use(s16 x, s16 y, s16 r, s16 h, u16 f);

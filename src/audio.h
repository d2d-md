#pragma once

#include <genesis.h>

enum soundid {
  SFX_FIRE_PLASMA = 0,
  SFX_SPLASH,
};

enum musicid {
  MUS_TEST = 0,
};

void snd_startup(void);
void snd_play_music(u16 idx);
void snd_play_sound(u16 idx);

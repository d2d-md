#include <genesis.h>
#include "audio.h"
#include "sound.h"
#include "music.h"

static const u8 *musiclib[] = {
  mus_test,
};

static const struct {
  const u8 id;
  const u8 *data;
  const u32 len;
  const u8 prio;
} sfxlib[] = {
  { 64, sfx_fire_plasma, sizeof(sfx_fire_plasma), 0 },
  { 65, sfx_splash, sizeof(sfx_splash), 0 },
};

void snd_startup(void) {
  // this assumes that ints are disabled
  Z80_loadDriver(Z80_DRIVER_XGM, TRUE);
  Z80_requestBus(TRUE);
  for (u16 i = 0; i < sizeof(sfxlib) / sizeof(sfxlib[0]); ++i)
    XGM_setPCMFast(sfxlib[i].id, sfxlib[i].data, sfxlib[i].len);
  Z80_releaseBus();
}

void snd_play_music(u16 idx) {
  XGM_startPlay(musiclib[idx]);
}

void snd_play_sound(u16 idx) {
  static const u16 chans[3] = { SOUND_PCM_CH2, SOUND_PCM_CH3, SOUND_PCM_CH4 };
  static u16 i = 0;
  XGM_startPlayPCM(sfxlib[idx].id, sfxlib[idx].prio, chans[i]);
  i = (i + 1) & 3;
}

#include <genesis.h>
#include "spr.h"
#include "sprite.h"

#define SPR_FLAGS SPR_FLAG_AUTO_VRAM_ALLOC | SPR_FLAG_AUTO_SPRITE_ALLOC | SPR_FLAG_AUTO_TILE_UPLOAD

static const struct {
  const SpriteDefinition *def;
  int pal;
} sprlib[] = {
  { &spr_player, PAL2 },
  {     &spr_fx, PAL3 },
};

void spr_startup(void) {
  SPR_init();
  VDP_setPalette(PAL2, spr_player.palette->data);
  VDP_setPalette(PAL3, spr_fx.palette->data);
}

Sprite *spr_get(u16 idx, s16 x, s16 y, bool flip) {
  return SPR_addSpriteExSafe(sprlib[idx].def, x, y, TILE_ATTR(sprlib[idx].pal, FALSE, FALSE, flip), 0, SPR_FLAGS);
}

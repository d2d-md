#include <genesis.h>

#include "spr.h"
#include "tiles.h"
#include "audio.h"
#include "map.h"
#include "thing.h"
#include "player.h"
#include "maps.h"

u16 g_ticker = 0;

static map_t maps[] = {
  DEFINE_MAP(0, 9, 63, map_map01),
};

static void do_input(void) {
  u16 value1 = JOY_readJoypad(JOY_1);
  u16 value2 = JOY_readJoypad(JOY_2);
  plr_controls(th_player[0], value1);
  plr_controls(th_player[1], value2);
}

static inline void update_scroll(void) {
  for (u16 i = 0; i < num_players; ++i) {
    VDP_setHorizontalScroll(g_cam[i].planeidx, -g_cam[i].x);
    VDP_setVerticalScroll(g_cam[i].planeidx, g_cam[i].y);
  }
}

#pragma GCC diagnostic ignored "-Wmain"

int main(u16 hard_reset) {
  if (!hard_reset)
    SYS_hardReset();

  SYS_disableInts();

  VDP_setScreenWidth320();
  VDP_setPaletteColors(0, (u16*)palette_black, 64);

  num_players = 2;

  spr_startup();
  tiles_startup();
  snd_startup();
  map_startup();

  map_load(maps + 0);

  th_player[0] = thing_spawn(TH_PLAYER, 40, 480, -1, &g_things[0]);
  th_player[1] = thing_spawn(TH_PLAYER, 40, 480, -1, &g_things[0]);

  g_cam[0].flags = g_cam[1].flags = 1;

  SPR_update();
  update_scroll();
  map_update();
  DMA_waitCompletion();

  SYS_enableInts();

  snd_play_music(0);

  while (1) {
    do_input();

    if (g_ticker & 1) {
      things_think();
      things_remove();
      map_update();
      things_draw();
      SPR_update();
    }

    ++g_ticker;
    SYS_doVBlankProcess();
    update_scroll();
  }

  return 0;
}

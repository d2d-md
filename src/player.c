#include <genesis.h>
#include "thing.h"
#include "player.h"
#include "audio.h"
#include "utils.h"
#include "map.h"
#include "switch.h"

#define PLR_RUNVEL 8
#define PLR_JUMPVEL 10
#define PLR_SWIMVEL 4

u16 num_players = 1;
thing_t *th_player[2] = { NULL };

void plr_controls(thing_t *p, u16 btns) {
  /*
  // noclip
  if (btns & BUTTON_RIGHT) {
    p->x += PLR_RUNVEL >> 3;
    p->dir = 1;
    p->ax = p->ay = 0;
    p->vx = p->vy = 0;
  } else if (btns & BUTTON_LEFT) {
    p->x -= PLR_RUNVEL >> 3;
    p->dir = -1;
    p->ax = p->ay = 0;
    p->vx = p->vy = 0;
  }

  if (btns & BUTTON_DOWN) {
    p->y += PLR_RUNVEL >> 3;
    p->ax = p->ay = 0;
    p->vx = p->vy = 0;
  } else if (btns & BUTTON_UP) {
    p->y -= PLR_RUNVEL >> 3;
    p->ax = p->ay = 0;
    p->vx = p->vy = 0;
  }
  */

  // walk
  if ((btns & BUTTON_RIGHT) && p->ax < PLR_RUNVEL) {
    p->ax += PLR_RUNVEL >> 3;
    p->dir = 1;
  } else if ((btns & BUTTON_LEFT) && p->ax > -PLR_RUNVEL) {
    p->ax -= PLR_RUNVEL >> 3;
    p->dir = -1;
  } else if (p->ax) {
    p->ax = Z_dec(p->ax, 1);
  }

  // jump or swim up
  if (btns & BUTTON_B) {
    if (p->flags & THF_INWATER)
      p->ay = -PLR_SWIMVEL;
    else if (p->flags & THF_ONGROUND)
      p->ay = -PLR_JUMPVEL;
  }

  // shoot
  if ((btns & BUTTON_A) && (g_ticker & 7) == 7) {
    snd_play_sound(SFX_FIRE_PLASMA);
    thing_t *th = thing_spawn(TH_TEST, p->x, p->y - 13, p->dir, p);
    if (th) th->dir = p->dir;
  }

  // use
  if ((btns & BUTTON_C) && (g_ticker & 7) == 7)
    sw_use(p->x, p->y, p->def->r, p->def->h, SWF_PL_PRESS);
}

#include <genesis.h>
#include "spr.h"
#include "gfx.h"
#include "map.h"

#include "maps.h"

static const struct {
//  const TileSet *set;
  const u32 *set;
//  const Palette *pal;
  const u16 *pal;
} tilelib[] = {
//  { &tiles_default, &pal_default },
  { (u32*)tiles_map01, (u16*)pal_map01 },
};

void tiles_startup(void) {
  VDP_setPlanSize(VDPW, VDPH);
}

void tiles_load(u16 idx) {
//  VDP_loadTileSet(tilelib[idx].set, TILE_USERINDEX, DMA);
  VDP_loadTileData(tilelib[idx].set, TILE_USERINDEX, 36, DMA);
//  VDP_setPalette(PAL0, tilelib[idx].pal->data);
//  VDP_setPalette(PAL1, tilelib[idx].pal->data);
  VDP_setPaletteColors(0, tilelib[idx].pal, 32);
}

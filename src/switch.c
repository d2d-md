#include <genesis.h>
#include "switch.h"
#include "map.h"

#define MTYPE(x) (x & ~MAP_FORCECLEAR)

static switch_t g_sw[MAX_SWITCHES];
static u16 numsw = 0;

void sw_load(switch_t *sw, u16 num) {
  numsw = num;
  memcpy(g_sw, sw, sizeof(switch_t)*num);
}

static bool toggle(u8 sx, u8 sy, u8 from, u8 to) {
  u8 *rowp = g_cells + sy*MAPW + sx;
  u8 *cellp;
  u8 ex, ey;

  if (MTYPE(*rowp) != from)
    return FALSE;

  // find x extents of the door
  ex = sx + 1;
  for (cellp = rowp - 1; sx && MTYPE(*cellp) == from; --sx, --cellp);
  for (cellp = rowp + 1; ex < MAPW && MTYPE(*cellp) == from; ++ex, ++cellp);

  // find y extents of the door
  ey = sy + 1;
  for (cellp = rowp - MAPW; sy && MTYPE(*cellp) == from; --sy, cellp -= MAPW);
  for (cellp = rowp + MAPW; ey < MAPH && MTYPE(*cellp) == from; ++ey, cellp += MAPW);

  // gotta blast
  const u8 dx = ex - sx;
  rowp = g_cells + sy*MAPW + sx;
  for (u8 y = sy; y < ey; ++y, rowp += MAPW)
    memset(rowp, to, dx);

  // update tiles that are on screen
  if (MTYPE(to) == MAP_OPENDOOR)
    map_clear_tile_rect(sx, sy, dx, ey-sy);
  else
    map_set_tile_rect(sx, sy, dx, ey-sy, g_map->tiles[tile_rowoff[sy] + sx]);

  return TRUE;
}

static void door_open(switch_t *sw) {
  if (toggle(sw->a, sw->b, MAP_DOOR, MAP_OPENDOOR | MAP_FORCECLEAR)) {
    g_cells[sw->b*MAPW + sw->a] = MAP_OPENDOOR; // clear the clear flag to keep it there
    map_set_tile(sw->a, sw->b, g_map->tiles[tile_rowoff[sw->b] + sw->a]);
  }
}

static bool door_shut(switch_t *sw) {
  toggle(sw->a, sw->b, MAP_OPENDOOR, MAP_DOOR);
  return FALSE;
}

static inline void sw_trigger(switch_t *sw, u16 f) {
  if (f & sw->flags) {
    switch (sw->type) {
      case SW_OPENDOOR:
        if (g_cells[sw->b*MAPW + sw->a] != MAP_DOOR)
          break;
        door_open(sw);
        sw->tm = 1;
        break;
      case SW_SHUTDOOR:
      case SW_SHUTTRAP:
        if (MTYPE(g_cells[sw->b*MAPW + sw->a]) != MAP_OPENDOOR)
          break;
        if (door_shut(sw)) {
          sw->tm = 1;
          sw->d = 0;
        } else {
          // if (!swsnd)
          //   swsnd = Z_sound(sndnoway, 128);
          sw->d = 2;
        }
        break;
    }
  }
}

void sw_use(s16 x, s16 y, s16 r, s16 h, u16 f) {
  s16 i, j, sx, sy, row;
  sx = (x - r) >> 3;
  sy = (y - h + 1) >> 3;
  if (sx < 0) sx = 0;
  if (sy < 0) sy = 0;
  x = (x + r) >> 3;
  y = (y    ) >> 3;
  if (x >= MAPW) x = MAPW - 1;
  if (y >= MAPH) y = MAPH - 1;
  row = sy*MAPW;
  u8 swid;
  for (j = sy; j <= y; ++j, row += MAPW) {
    for (i = sx; i <= x; ++i) {
      swid = g_map->swmap[row + i];
      if (swid != 0xFF)
        sw_trigger(g_sw + swid, f);
    }
  }
}

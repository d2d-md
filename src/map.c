#include <genesis.h>
#include "map.h"
#include "tiles.h"
#include "player.h"
#include "switch.h"
#include "maps.h"

u8 g_cells[MAPH*MAPW] = { 0 };

map_t *g_map = NULL;
cam_t g_cam[MAX_PLAYERS] = {
  // first player's camera is always at y offset 0
  { .planeidx = BG_A, .sy = 0,                      .sty = 0,                  .sh = VIEWPIXH, .sth = VIEWH },
  // second player's camera is in the bottom half of the screen
  { .planeidx = BG_B, .sy = (SCREENPIXH >> 1) + 16, .sty = (SCREENH >> 1) + 2, .sh = VIEWPIXH, .sth = VIEWH },
};

static const u16 bg_basetile = TILE_ATTR_FULL(PAL0, 0, 0, 0, TILE_USERINDEX);
static const u16 fg_basetile = TILE_ATTR_FULL(PAL0, 1, 0, 0, TILE_USERINDEX);
const u16 tile_rowoff[MAPW] = {
     0,  100,  200,  300,  400,  500,  600,  700,  800,  900, 
  1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 
  2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 
  3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700, 3800, 3900, 
  4000, 4100, 4200, 4300, 4400, 4500, 4600, 4700, 4800, 4900, 
  5000, 5100, 5200, 5300, 5400, 5500, 5600, 5700, 5800, 5900, 
  6000, 6100, 6200, 6300, 6400, 6500, 6600, 6700, 6800, 6900, 
  7000, 7100, 7200, 7300, 7400, 7500, 7600, 7700, 7800, 7900, 
  8000, 8100, 8200, 8300, 8400, 8500, 8600, 8700, 8800, 8900, 
  9000, 9100, 9200, 9300, 9400, 9500, 9600, 9700, 9800, 9900, 
};

static u16 empty_row[VDPW] = { 0 };

void map_startup(void) {
  for (u16 i = 0; i < MAX_PLAYERS; ++i) {
    if (g_cam[i].planeidx == BG_A)
      g_cam[i].plane = VDP_BG_A;
    else
      g_cam[i].plane = VDP_BG_B;
  }
}

void map_load(map_t *map) {
  tiles_load(map->tilemap);
  g_map = map;
  memcpy(g_cells, map->map, sizeof(g_cells));
  sw_load(map->sw, map->numsw);
  things_load(map->th, map->numth);

  if (num_players > 1) {
    // player 1's camera is only half height if there's multiple cameras
    g_cam[0].sh = VIEWPIXH;
    g_cam[0].sth = VIEWH;
  } else {
    // if there's only a single camera, it takes up the whole screen
    g_cam[0].sh = SCREENPIXH - 16;
    g_cam[0].sth = SCREENH - 2;
  }
}

static inline void cam_clear_row(cam_t *cam, const u16 vty) {
  DMA_queueDmaFast(DMA_VRAM, empty_row, cam->plane + (vty << (6 + 1)), VDPW, 2);
}

static inline void cam_upload_row(cam_t *cam, const u16 vty, const u16 ty) {
  u16 *buf = DMA_allocateTemp(VDPW);
  DMA_queueDmaFast(DMA_VRAM, buf, cam->plane + (vty << (6 + 1)), VDPW, 2);
  register u16 *datap = g_map->tiles + tile_rowoff[ty] + cam->tx;
  register u8 *cellp = g_cells + tile_rowoff[ty] + cam->tx;
  register u16 rx = cam->tx;
  for (register u16 i = VDPW; i; --i) {
    rx &= (VDPW - 1);
    buf[rx++] = (*cellp & MAP_FORCECLEAR) ? bg_basetile : bg_basetile + *datap;
    ++datap;
    ++cellp;
  }
}

static inline void cam_upload_column(cam_t *cam, const u16 vtx, const u16 tx) {
  u16 *buf = DMA_allocateTemp(cam->sth);
  // the column might be split by the plane border
  const u16 vty0 = (cam->sty + cam->ty) & (VDPH - 1);
  const u16 vty1 = vty0 + cam->sth;
  if (vty1 > VDPH) {
    // it's split, have to do two DMAs
    const u16 vth0 = VDPH - vty0;
    const u16 vth1 = vty1 - VDPH;
    DMA_queueDmaFast(DMA_VRAM, buf +    0, cam->plane + (((vty0 << 6) + vtx) << 1), vth0, VDPW << 1);
    DMA_queueDmaFast(DMA_VRAM, buf + vth0, cam->plane + (vtx                 << 1), vth1, VDPW << 1);
  } else {
    // it's in the middle of the plane, do one DMA
    DMA_queueDmaFast(DMA_VRAM, buf, cam->plane + (((vty0 << 6) + vtx) << 1), cam->sth, VDPW << 1);
  }
  // fill the buffer
  register u16 *datap = g_map->tiles + tile_rowoff[cam->ty] + tx;
  register u8 *cellp = g_cells + tile_rowoff[cam->ty] + tx;
  register u16 i;
  for (i = 0; i < cam->sth; ++i) {
    buf[i] = (*cellp & MAP_FORCECLEAR) ? bg_basetile : bg_basetile + *datap;
    datap += MAPW;
    cellp += MAPW;
  }
}

static inline void view_update(cam_t *cam, thing_t *plr) {
  if (plr) {
    register s16 cx = plr->x - 160;
    register s16 cy = plr->y - 64;
    if (cx < 0) cx = 0;
    else if (cx > MAPPIXW - VIEWPIXW) cx = MAPPIXW - VIEWPIXW;
    if (cy < 0) cy = 0;
    else if (cy > MAPPIXH - cam->sh) cy = MAPPIXH - cam->sh;
    if (cam->x != cx || cam->y != cy) {
      cam->x = cx;
      cam->y = cy;
      cam->tx = cx >> 3;
      cam->ty = cy >> 3;
    }
  }
}

static inline void cam_update(cam_t *cam, thing_t *plr) {
  u16 otx = cam->tx;
  u16 oty = cam->ty;

  view_update(cam, plr);

  u16 tx = cam->tx;
  u16 ty = cam->ty;

  s16 dx = tx - otx;
  s16 dy = ty - oty;

  if (dx == 0 && dy == 0)
    return;

  // can only scroll a maximum of one screen + borders
  if (dx > VIEWW) {
    otx += dx - VIEWW;
    dx = VIEWW;
    dy = 0; // full screen update, skip rows
  } else if (dx < -VIEWW) {
    otx += dx + VIEWW;
    dx = -VIEWW;
    dy = 0; // full screen update, skip rows
  } else if (dy > cam->sth) {
    oty += dy - cam->sth;
    dy = cam->sth;
    dx = 0; // full screen update, skip cols
  } else if (dy < -cam->sth) {
    oty += dy + cam->sth;
    dy = -cam->sth;
    dx = 0; // full screen update, skip cols
  }

  if (dx > 0) {
    // upload on the right, columns need no clearing
    tx = otx + VIEWW;
    while (dx--) {
      // cam_clear_column(cam, otx & (VDPW - 1));
      cam_upload_column(cam, tx & (VDPW - 1), tx);
      ++tx;
      // ++otx;
    }
  } else {
    // upload on the left, columns need no clearing
    // tx = otx + VIEWW;
    while (dx++) {
      // --tx;
      --otx;
      cam_upload_column(cam, otx & (VDPW - 1), otx);
      // cam_clear_column(cam, tx & (VDPW - 1));
    }
  }

  if (dy > 0) {
    // upload on the bottom, clear on the top
    ty = oty + cam->sth;
    while (dy--) {
      cam_clear_row(cam, (cam->sty + oty) & (VDPH - 1));
      cam_upload_row(cam, (cam->sty + ty) & (VDPH - 1), ty);
      ++ty;
      ++oty;
    }
  } else {
    // upload on the top, clear on the bottom
    ty = oty + cam->sth;
    while (dy++) {
      --ty;
      --oty;
      cam_upload_row(cam, (cam->sty + oty) & (VDPH - 1), oty);
      cam_clear_row(cam, (cam->sty + ty) & (VDPH - 1));
    }
  }
}

void map_update(void) {
  for (u16 i = 0; i < num_players; ++i)
    cam_update(&g_cam[i], th_player[i]);
}

static inline s16 tile_in_camera(const cam_t *cam, const u16 tx, const u16 ty) {
  return (tx >= cam->tx && tx <= cam->tx + VIEWW &&
          ty >= cam->ty && ty <= cam->ty + cam->sth);
}

void map_clear_tile(u16 tx, u16 ty) {
  for (u16 i = 0; i < num_players; ++i)
    if (tile_in_camera(&g_cam[i], tx, ty))
      VDP_setTileMapXY(g_cam[i].planeidx, bg_basetile, tx & (VDPW - 1), (ty + g_cam[i].sty) & (VDPH - 1));
}

void map_set_tile(u16 tx, u16 ty, u16 tile) {
  for (u16 i = 0; i < num_players; ++i)
    if (tile_in_camera(&g_cam[i], tx, ty))
      VDP_setTileMapXY(g_cam[i].planeidx, fg_basetile + tile, tx & (VDPW - 1), (ty + g_cam[i].sty) & (VDPH - 1));
}

void map_clear_tile_rect(s16 tx, s16 ty, s16 w, s16 h) {
  // TODO: just clip the rect and do a fill
  for (; tx < tx + w; ++tx)
    for (; ty < ty + h; ++ty)
      map_clear_tile(tx, ty);
}

void map_set_tile_rect(s16 tx, s16 ty, s16 w, s16 h, u16 tile) {
  // TODO: just clip the rect and do a fill
  for (; tx < tx + w; ++tx)
    for (; ty < ty + h; ++ty)
      map_set_tile(tx, ty, tile);
}

#include <genesis.h>
#include "thing.h"
#include "spr.h"
#include "audio.h"
#include "map.h"
#include "player.h"
#include "utils.h"

#define MAX_YVEL 15

static const thingclass_t classes[] = {
  {         0,   0,   0,   0,   0,   0,                                         0 }, // world
  { TH_PLAYER,   0,   8,  26, -12, -31, THF_MOVING | THF_GRAVITY | THF_TAKEDAMAGE },
  {   TH_TEST,   1,  12,   8, -12,  -8,                                THF_MOVING },
};

static thing_t thingpool[MAX_THINGS];
thing_t *g_things = &thingpool[0]; // thing 0 is always worldspawn

thing_t *thing_spawn(u16 class, s16 x, s16 y, s16 dir, thing_t *chain) {
  for (s16 i = 1; i < MAX_THINGS; i++) {
    if (thingpool[i].flags == 0) {
      thing_t *th = thingpool + i;
      const thingclass_t *tc = classes + class;
      th->def = tc;
      th->x = th->ox = x;
      th->y = th->oy = y;
      th->flags = tc->flags;
      for (u16 j = 0; j < num_players; ++j)
        th->spr[j] = spr_get(tc->spr, x - g_cam[j].x + tc->offx, y - g_cam[j].y + tc->offy, dir == -1);
      th->next = chain->next;
      th->prev = chain;
      th->dir = dir;
      if (th->next) th->next->prev = th;
      chain->next = th;
      return th;
    }
  }
  return NULL;
}

void thing_free(thing_t *th) {
  if (th->next) th->next->prev = th->prev;
  if (th->prev) th->prev->next = th->next;
  th->def = NULL;
  th->vx = 0;
  th->vy = 0;
  th->ax = 0;
  th->ay = 0;
  th->anim = 0;
  th->frame = 0;
  for (u16 j = 0; j < MAX_PLAYERS; ++j) {
    if (th->spr[j]) {
      SPR_releaseSprite(th->spr[j]);
      th->spr[j] = NULL;
    }
  }
}

void thing_draw(thing_t *th) {
  for (u16 i = 0; i < num_players; ++i) {
    if (th->spr[i]) {
      const s16 x = th->x + th->def->offx - g_cam[i].x;
      const s16 y = th->y + th->def->offy - g_cam[i].y + g_cam[i].sy;
      const s16 sy = g_cam[i].sy - 32;
      const s16 ey = g_cam[i].sy + g_cam[i].sh + 32;
      if (x >= -32 && y >= sy && x < 352 && y < ey) {
        SPR_setVisibility(th->spr[i], VISIBLE);
        SPR_setAnim(th->spr[i], th->anim);
        SPR_setHFlip(th->spr[i], th->dir == -1);
        SPR_setPosition(th->spr[i], x, y);
      } else {
        SPR_setVisibility(th->spr[i], HIDDEN);
      }
    }
  }
}

void things_load(mapthing_t *th, u16 num) {
  // TODO
}

// thing physics

static inline s16 Z_canstand(s16 x, s16 y, s16 r) {
  s16 i = (x - r) >> 3;
  x = (x + r) >> 3;
  y = (y + 1) >> 3;
  if (y >= MAPH || y < 0) return 0;
  if (i < 0) i = 0;
  if (x >= MAPW) x = MAPW - 1;
  const s16 row = y*MAPW;
  u8 t;
  for (; i <= x; ++i) {
    t = g_cells[row + i];
    if (t == MAP_WALL || t == MAP_STEP || t == MAP_DOOR)
      return 1;
  }
  return 0;
}

static inline s16 Z_hitceil(s16 x, s16 y, s16 r, s16 h) {
  s16 i = (x - r) >> 3;
  x = (x + r) >> 3;
  y = (y - h + 1) >> 3;
  if (y >= MAPH || y < 0) return 0;
  if (i < 0) i = 0;
  if (x >= MAPW) x = MAPW - 1;
  const s16 row = y*MAPW;
  u8 t;
  for (; i <= x; ++i) {
    t = g_cells[row + i];
    if (t == MAP_WALL || t == MAP_DOOR)
      return 1;
  }
  return 0;
}

static inline s16 Z_canfit(s16 x, s16 y, s16 r, s16 h) {
  s16 i, j, sx, sy, row;
  sx = (x - r) >> 3;
  sy = (y - h + 1) >> 3;
  if (sx < 0) sx = 0;
  if (sy < 0) sy = 0;
  x = (x + r) >> 3;
  y = (y    ) >> 3;
  if (x >= MAPW) x = MAPW - 1;
  if (y >= MAPH) y = MAPH - 1;
  row = sy*MAPW;
  u8 t;
  for (j = sy; j <= y; ++j, row += MAPW) {
    for (i = sx; i <= x; ++i) {
      t = g_cells[row + i];
      if (t == MAP_WALL || t == MAP_DOOR)
        return 0;
    }
  }
  return 1;
}

static inline s16 Z_inwater(int x, int y, int r, int h) {
  s16 i, j, sx, sy, row;
  u8 t;
  sx = (x - r) >> 3;
  sy = (y - h + 1) >> 3;
  if (sx < 0) sx = 0;
  if (sy < 0) sy = 0;
  x = (x + r) >> 3;
  y = (y - (h >> 1)) >> 3;
  if (x >= MAPW) x = MAPW - 1;
  if (y >= MAPH) y = MAPH - 1;
  row = sy*MAPW;
  for (j = sy; j <= y; ++j, row += MAPW) {
    for (i = sx; i <= x; ++i) {
      t = g_cells[row + i];
      if (t >= MAP_WATER && t <= MAP_ACID2)
        return 1;
    }
  }
  return 0;
}

static inline u16 thing_move(thing_t *p) {
  const s16 r = p->def->r;
  const s16 h = p->def->h;
  s16 x = p->ox = p->x;
  s16 y = p->oy = p->y;
  s16 xv, yv, lx, ly;
  s16 oldwater, newwater;
  u16 st = 0;

  // TODO: lifts

  if (p->flags & THF_GRAVITY)
    if (++p->ay > MAX_YVEL)
      --p->ay;

  #define wvel(v) \
    if ((xv = abs(v) + 1) > 5) v = Z_dec(v, (xv >> 1) - 2)

  if ((oldwater = Z_inwater(x, y, r, h))) {
    wvel(p->vx);
    wvel(p->vy);
    wvel(p->ax);
    wvel(p->ay);
  }

  #undef wvel

  p->vx = Z_dec(p->vx, 1);
  p->vy = Z_dec(p->vy, 1);

  xv = p->ax + p->vx;
  yv = p->ay + p->vy;
  p->flags &= ~THF_ONGROUND;
  while (xv || yv) {
    if (x < -100 || x >= MAPPIXW + 100 || y < -100 || y >= MAPPIXH + 100) {
       st |= MOVE_FALLOUT;
       return st;
    }
    lx = x;
    x += (abs(xv) <= 7) ? xv : ((xv > 0) ? 7 : -7);
    if (!Z_canfit(x, y, r, h)) {
      if (xv == 0)
        x = lx;
      else if (xv < 0)
        x = ((lx - r) & 0xFFF8) + r;
      else
        x = ((lx + r) & 0xFFF8) - r + 7;
      xv = p->ax = p->vx = 0;
      st |= MOVE_HITWALL;
    }
    xv -= (abs(xv) <= 7) ? xv : ((xv > 0) ? 7 : -7);

    ly = y;
    y += (abs(yv) <= 7) ? yv : ((yv > 0) ? 7 : -7);
    if (yv >= 8)
      --y;
    if (yv < 0 && Z_hitceil(x, y, r, h)) {
      y = ((ly - h + 1) & 0xFFF8) + h - 1;
      yv = p->vy = 1;
      p->ay = 0;
      st |= MOVE_HITCEIL;
    }
    if (yv > 0 && Z_canstand(x, y, r)) {
      y = ((y + 1) & 0xFFF8) - 1;
      yv = p->ay = p->vy = 0;
      p->flags |= THF_ONGROUND;
    }
    yv -= (abs(yv) <= 7) ? yv : ((yv > 0) ? 7 : -7);
  }

  p->x = x;
  p->y = y;

  newwater = Z_inwater(x, y, r, h);
  if (newwater) {
    if (!oldwater) st |= MOVE_HITWATER;
    p->flags |= THF_INWATER;
  } else if (oldwater) {
    st |= MOVE_HITAIR;
    p->flags &= ~THF_INWATER;
  }

  return st;
}

void thing_think(thing_t *th) {
  // run physics if needed
  if (th->flags & THF_MOVING) {
    u16 st = thing_move(th);
    // fell out of map, die
    if (st & MOVE_FALLOUT) {
      th->flags |= THF_REMOVED;
      return;
    }
    if (st & MOVE_HITWATER) snd_play_sound(SFX_SPLASH);
    // test missile: fly, die when collided with something
    if (th->def->class == TH_TEST) {
      if (st & MOVE_HIT) {
        th->flags |= THF_REMOVED;
        return;
      }
      th->ax = th->dir * 8;
    }
  }

  if (th->def->class == TH_PLAYER)
    th->anim = (th->ax) ? 3 : 0;
}
